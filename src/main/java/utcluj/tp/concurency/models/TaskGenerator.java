package utcluj.tp.concurency.models;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Random; 
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;


public class TaskGenerator implements Callable<Task> {
	private Duration maxProcessingPeriod;
	private Duration minProcessingPeriod;
	private Duration minIntervalBetweenTasks;
	private Duration maxIntervalBetweenTasks;
	
	/*Static variable - task counter*/
	public static AtomicInteger numberOfGeneratedTasks = new AtomicInteger(0);
	
	/*Constructor*/
	public TaskGenerator(long minProcessingPeriodInSeconds, long maxProcessingPeriodInSeconds, 
						 long minIntervalBetweenTasksInSeconds, long maxIntervalBetweenTasksInSeconds) {
		this.maxProcessingPeriod = Duration.ofSeconds(maxProcessingPeriodInSeconds);
		this.minProcessingPeriod = Duration.ofSeconds(minProcessingPeriodInSeconds);
		this.minIntervalBetweenTasks = Duration.ofSeconds(minIntervalBetweenTasksInSeconds);
		this.maxIntervalBetweenTasks = Duration.ofSeconds(maxIntervalBetweenTasksInSeconds);
	}
	
	/*Methods*/
	private Task generate() {
		Random random = new Random();

		/*Generating a random processing time for a task*/
		int higherBound = (int) maxProcessingPeriod.getSeconds();
		int lowerBound = (int) minProcessingPeriod.getSeconds();
		int processingTime = random.nextInt(higherBound - lowerBound + 1) + lowerBound;
																																																																																							
		return new Task("Task " + numberOfGeneratedTasks.getAndIncrement(), Duration.ofSeconds(processingTime), LocalTime.now());
		
	}

	public Task call() throws Exception {
		return generate();
	}
	
	public int getRandomIntervalBetweenTasks() {
		Random random = new Random();
		
		/*Generating a random interval between two tasks*/
		int higherBound = (int) maxIntervalBetweenTasks.getSeconds();
		int lowerBound = (int) minIntervalBetweenTasks.getSeconds();
		int intervalntervalBetweenTasks = random.nextInt(higherBound - lowerBound + 1) + lowerBound;
		return intervalntervalBetweenTasks;
	}
	 
	
}
