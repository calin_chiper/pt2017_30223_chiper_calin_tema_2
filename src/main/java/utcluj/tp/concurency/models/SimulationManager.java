package utcluj.tp.concurency.models;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimulationManager {
	private long pushPeriod;
	private int numberOfServers;
	private long maxProcessingPeriod;
	private long minProcessingPeriod;
	private int numberOfTaskGenerators;
	private long minIntervalBetweenTasks;
	private long maxIntervalBetweenTasks;
	private List<Server> servers;
	private ExecutorService executor;
	private BlockingQueue<Task> queue;
	private List<Pusher> taskPushers;

	public SimulationManager(long pushPeriod, int numberOfServers, long maxProcessingPeriod, long minProcessingPeriod,
			int numberOfTaskGenerators, long minIntervalBetweenTasks, long maxIntervalBetweenTasks) {
		this.pushPeriod = pushPeriod;
		this.numberOfServers = numberOfServers;
		this.maxProcessingPeriod = maxProcessingPeriod;
		this.minProcessingPeriod = minProcessingPeriod;
		this.numberOfTaskGenerators = numberOfTaskGenerators;
		this.minIntervalBetweenTasks = minIntervalBetweenTasks;
		this.maxIntervalBetweenTasks = maxIntervalBetweenTasks;
		this.servers = new ArrayList<Server>();
		int totalThreads = this.numberOfServers + this.numberOfTaskGenerators;
		this.executor = Executors.newFixedThreadPool(totalThreads);
		this.queue = new ArrayBlockingQueue<Task>(20);
		this.taskPushers = new ArrayList<>();
	}

	public void finish() {
		
		this.executor.shutdownNow();
		System.out.println("Terminated");

	}

	public void spawnServers() {
		/* Spawn servers */
		for (int thread = 0; thread < this.numberOfServers; thread++) {
			Server server = new Server(queue);
			servers.add(server);
			executor.submit(server);
		}

	}

	public void spawnGenerators() {
		/* Spawn task generators */
		for (int thread = 0; thread < this.numberOfTaskGenerators; thread++) {
			Pusher pusher = new Pusher(
					queue, new TaskGenerator(this.minProcessingPeriod, this.maxProcessingPeriod,
							this.minIntervalBetweenTasks, this.maxIntervalBetweenTasks),
					Duration.ofSeconds(pushPeriod));
			taskPushers.add(pusher);
			executor.execute(pusher);
		}

	}

	public List<Server> getServers() {
		return this.servers;
	}
	
	public List<Pusher> getTaskPushers() {
		return this.taskPushers;
	}

	public ExecutorService getExecutor() {
		return this.executor;
	}

	public void reset() {
		Server.numberOfServers.set(0);
		TaskGenerator.numberOfGeneratedTasks.set(0);
		this.servers.clear();
		this.queue.clear();
	}

	public String getServersStatusCode() {
		String code = "";
		for (Server server : this.servers) {
			if (server.isBusy()) {
				code += "1";
			} else {
				code += "0";
			}
		}
		return code;
	}

	public List<Task> getWaitingTasks() {
		Object[] tasks = this.queue.toArray();
		List<Task> listOfTasks = new LinkedList<Task>();
		for (int t = 0; t < tasks.length; t++) {
			listOfTasks.add((Task) tasks[t]);

		}
		System.out.println(listOfTasks);
		return listOfTasks;
	}

}
