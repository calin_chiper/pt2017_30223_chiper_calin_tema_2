package utcluj.tp.concurency.models;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


public class Tracker  {
	
	/*Singleton*/
	private static final Tracker TRACKER = new Tracker();
	
	/*Key = Task name ; Value = Computed waiting time*/
	private ConcurrentHashMap<String, Long> waitingTimeCounter;
	private List<Duration> listOfFinishedTime; 
	private AtomicInteger totalProcessingPeriod;
	
	
	/*Constructor*/
	private Tracker() {
		this.waitingTimeCounter = new ConcurrentHashMap<String, Long>();
		this.listOfFinishedTime = new ArrayList<Duration>();
		this.totalProcessingPeriod = new AtomicInteger(0);
	}
	
	/*Methods*/
	public static Tracker getInstance() {
		return TRACKER;
	}
	
	public void track(Task task) {
		long startTime = System.currentTimeMillis();
		totalProcessingPeriod.addAndGet((int) task.getProcessingTime().getSeconds());
		waitingTimeCounter.put(task.getTaskName(), startTime);
	}
	
	public Duration getWaitingTime(Task task) {
		long finishTime = System.currentTimeMillis();
		long startTime = waitingTimeCounter.remove(task.getTaskName());
		long waitingTime = (finishTime - startTime) / 1000;	//conversion to seconds
		listOfFinishedTime.add(Duration.ofSeconds(waitingTime));
		return Duration.ofSeconds(waitingTime);
	}
	
	public Duration getAverageWaitingTime() {
		long totalTime = 0;
		for(Duration duration: listOfFinishedTime) {
			if(duration != null) {
				totalTime += duration.getSeconds();
			}
			
		}
		return Duration.ofSeconds( totalTime / listOfFinishedTime.size());
	}
	
	public int getAverageProcessingTime() {
		return this.totalProcessingPeriod.get() / listOfFinishedTime.size() ;
	}
	
	public void setTotalProcessingPeriod(int newValue) {
		this.totalProcessingPeriod.set(newValue);
	}
	
	
	
}
