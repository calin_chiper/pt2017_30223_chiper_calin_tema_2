package utcluj.tp.concurency.models;

import java.time.Duration;
import java.time.LocalTime;


public class Task  {
	private String taskName;
	private Duration processingTime;
	private LocalTime arrivalTime;	//when is scheduled 
	private LocalTime finishTime;
	private Duration waitingPeriod;
	
	
	/*Constructor*/
	public Task(String taskName, Duration processingTime, LocalTime arrivalTime) {
		this.taskName = taskName;
		this.processingTime = processingTime;
		this.arrivalTime = arrivalTime;
	}
	
	/*Methods*/
	
	public String getTaskName() {
		return this.taskName;
	}
	
	public LocalTime getFinishTime() {
		finishTime = LocalTime.from(arrivalTime);
		finishTime = finishTime.plusNanos(processingTime.toNanos())
				  			   .plusNanos(waitingPeriod.toNanos());
		return finishTime;	
	}
	
	public Duration getProcessingTime() {
		return this.processingTime;
	}
	
	public LocalTime getArrivalTime() {
		return this.arrivalTime;
	}
	
	public void setWaitingPeriod(Duration duration) {
		this.waitingPeriod = duration;
	}
	
	public Duration getWaitingPeriod() {
		return this.waitingPeriod;
	}
	
}
