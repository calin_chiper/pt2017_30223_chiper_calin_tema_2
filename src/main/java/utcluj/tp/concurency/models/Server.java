package utcluj.tp.concurency.models;

import java.time.Duration;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;



public class Server implements Callable<Server> {

	private String serverName;
	private BlockingQueue<Task> queue;
	private byte status;
	private AtomicReference<Task> currentTaskOnServer;
	private AtomicInteger numberOfProcessedTasks;
	
	/*Static variables*/
	public static AtomicInteger numberOfServers = new AtomicInteger(0);
	private static final byte BUSY = 0;
	private static final byte IDLE = 1;
	
	/*Constructor*/
	public Server(BlockingQueue<Task> sharedQueue) {
		this.serverName = "Server " + numberOfServers.getAndIncrement();
		this.queue = sharedQueue;
		this.status = IDLE;
		this.numberOfProcessedTasks = new AtomicInteger(0);
	}
	
	/*Methods*/
	public Server call() {
		try {
			processTask();
		} catch (InterruptedException e) {
			System.out.println("Server: Forced Interruption");
		}
		return this;
	}

	private void processTask() throws InterruptedException {
		while(true) {
			Task task = queue.poll(5, TimeUnit.SECONDS);
			if(task != null) {
				Duration waitingTime = Tracker.getInstance().getWaitingTime(task);
				task.setWaitingPeriod(waitingTime);	
				simulateProcessing(task);
			} else {
				break;
			}	
		}
	}

	private void simulateProcessing(Task task) throws InterruptedException {
		this.currentTaskOnServer = new AtomicReference<Task>(task);
		this.numberOfProcessedTasks.incrementAndGet();
		this.status = BUSY;
		Thread.sleep(task.getProcessingTime().toMillis());
		this.status = IDLE;
	}
	
	
	  
	public boolean isBusy() {
		if(this.status == BUSY) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getServerName() {
		return this.serverName;
	}
	
	public Task getCurrentTaskOnServer() {
		return this.currentTaskOnServer.get();
	}
	
	public int getNumberOfProcessedTasks() {
		return this.numberOfProcessedTasks.get();
	}
	

	
}
