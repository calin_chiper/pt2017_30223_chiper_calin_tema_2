package utcluj.tp.concurency.models;

import java.time.Duration;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Pusher implements Runnable {
	private final BlockingQueue<Task> queue;
	private ScheduledExecutorService scheduler;
	private TaskGenerator taskGenerator;
	private Duration pushingPeriod;

	/* Constructor */
	public Pusher(BlockingQueue<Task> sharedQueue, TaskGenerator taskGenerator, Duration pushingPeriod) {
		this.queue = sharedQueue;
		this.taskGenerator = taskGenerator;
		this.scheduler = Executors.newScheduledThreadPool(1); // One scheduler
		this.pushingPeriod = pushingPeriod;
	}

	/* Methods */
	public void run() {
		/* Pushing period starts */
		long startTime = System.currentTimeMillis();
		while (true) {
			try {
				Task task = getIncomingTask();
				pushToServer(task);
				if (isDone(startTime)) {
					this.scheduler.shutdown();
					break;
				}

			} catch (InterruptedException e) {
				System.out.println("Pusher: Forced interruption");
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean isDone(long startTime) {
		if ((System.currentTimeMillis() - startTime) / 1000 >= this.pushingPeriod.getSeconds()) {
			return true;
		}
		return false;
	}
	


	private void pushToServer(Task task) throws InterruptedException {
		Tracker.getInstance().track(task);
		this.queue.put(task);
	}

	private Task getIncomingTask() throws InterruptedException, ExecutionException {
		Task generatedTask = scheduler
				.schedule(taskGenerator, this.taskGenerator.getRandomIntervalBetweenTasks(), TimeUnit.SECONDS).get();
		return generatedTask;
	}
}
