package utcluj.tp.concurency.controller;

import java.net.URL;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.ResourceBundle;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.scene.text.Font;
import javafx.stage.Stage;
import utcluj.tp.concurency.models.SimulationManager;
import utcluj.tp.concurency.models.Tracker;

public class MainController implements Initializable {

	/* UI Injected components */
	@FXML
	private BorderPane rootPane;

	@FXML
	private JFXComboBox<Integer> nrOfServersComboBox;

	@FXML
	private JFXComboBox<Integer> nrOfTaskGeneratorsComboBox;

	@FXML
	private JFXSlider timeChooser;

	@FXML
	private JFXToggleButton startStopToggleButton;

	@FXML
	private JFXTextField minProcessingTimeTextField;

	@FXML
	private JFXTextField maxProcessingTimeTextField;

	@FXML
	private JFXTextField minIntervalTextField;

	@FXML
	private JFXTextField maxIntervalTextField;

	@FXML
	private Label remainingTimeLabel;

	@FXML
	private VBox mainPanel;

	@FXML
	private HBox serverImages;

	@FXML
	private HBox serversStatus;

	@FXML
	private AnchorPane results;

	@FXML
	private MenuItem closeMenuItem;

	@FXML
	private Label simulationTimeLabel;
	
	@FXML
    private Label averageWaitingTimeLabel;

    @FXML
    private Label averageProcessingTimeLabel;



	/* Non-UI components */

	private SimulationManager simulationManager;

	private Thread backgroundThread;

	private Thread counterThread;

	private Thread simulationTime;

	private HashMap<Integer, String> lastConsoleMessage;

	private boolean counting;

	private boolean running = true;

	/* Methods */
	public void initialize(URL location, ResourceBundle resources) {
		initializeMenu();
		initializeActionEvents();
	}

	private void initializeActionEvents() {

		/* Setting the number of server to show */
		nrOfServersComboBox.setOnAction(event -> {
			showServers();
		});

		closeMenuItem.setOnAction(event -> {
			Stage primaryStage = (Stage) this.rootPane.getScene().getWindow();
			if (simulationManager != null) {
				running = false;
				stopSimulationNow();
				counterThread.interrupt();
				simulationTime.interrupt();
			}
			primaryStage.close();
		});

		/* Start/Stop toggle button */
		startStopToggleButton.setOnAction(event -> {
			if (startStopToggleButton.isSelected()) {
				// startStopToggleButton.setText("Stop");
				startSimulation();
				startCounter();
				countSimulationTime();
				disableMenu(true);
				results.setVisible(false);
				counting = true;
			} else {
				startStopToggleButton.setText("Start");
				disableMenu(false);
				stopSimulationNow();
				counting = false;

			}
		});

	}

	private void disableMenu(boolean value) {
		nrOfServersComboBox.setDisable(value);
		nrOfTaskGeneratorsComboBox.setDisable(value);
		maxIntervalTextField.setDisable(value);
		minIntervalTextField.setDisable(value);
		maxProcessingTimeTextField.setDisable(value);
		minProcessingTimeTextField.setDisable(value);
		timeChooser.setDisable(value);
	}

	private void showServers() {
		serverImages.getChildren().clear();
		serversStatus.getChildren().clear();
		lastConsoleMessage = new HashMap<>();
		for (int serverIndex = 0; serverIndex < nrOfServersComboBox.getValue(); serverIndex++) {
			// setting up the images of servers
			ImageView imageView = new ImageView(new Image("/images/server-green.png"));
			imageView.setFitHeight(128);
			imageView.setFitWidth(128);
			serverImages.getChildren().add(imageView);
			TextArea textArea = new TextArea();
			textArea.setFont(new Font(10));
			textArea.setPrefWidth(128);
			textArea.setPrefHeight(256);
			textArea.setEditable(false);
			textArea.setText("Server " + serverIndex + " console: \n");
			lastConsoleMessage.put(serverIndex, "Server " + serverIndex + " console: \n");
			serversStatus.getChildren().add(textArea);
		}
	}

	private void startSimulation() {
		Task<Void> backgroundTask = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				long pushPeriod = (long) timeChooser.getValue();
				int numberOfServers = nrOfServersComboBox.getValue();
				int numberOfPushers = nrOfTaskGeneratorsComboBox.getValue();
				long minProcessingTime = Long.parseLong(minProcessingTimeTextField.getText());
				long maxProcessingTime = Long.parseLong(maxProcessingTimeTextField.getText());
				long minIntervalBetweenTasks = Long.parseLong(minIntervalTextField.getText());
				long maxIntervalBetweenTasks = Long.parseLong(maxIntervalTextField.getText());

				simulationManager = new SimulationManager(pushPeriod, numberOfServers, maxProcessingTime,
						minProcessingTime, numberOfPushers, minIntervalBetweenTasks, maxIntervalBetweenTasks);
				simulationManager.reset();
				simulationManager.spawnGenerators();
				simulationManager.spawnServers();

				simulationManager.getExecutor().shutdown();

				while (!simulationManager.getExecutor().isTerminated()
						|| !remainingTimeLabel.getText().equals("0 seconds")) {
					updateMessage(simulationManager.getServersStatusCode());

					if (!running) {
						break;
					}
				}

				counting = false;
				stopSimulator();
				return null;
			}

		};

		backgroundTask.messageProperty().addListener((obs, oldValue, newValue) -> {
			updateServersUI(newValue);
			updateServersStatus(newValue);
		});

		this.backgroundThread = new Thread(backgroundTask);
		this.backgroundThread.start();
	}


	private void countSimulationTime() {
		Task<Void> counter = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				int simulationTime = 0;
				while (counting) {
					updateMessage(simulationTime++ + " seconds");
					Thread.sleep(1000);
				}
				return null;
			}

		};
		simulationTimeLabel.textProperty().bind(counter.messageProperty());
		this.simulationTime = new Thread(counter);
		this.simulationTime.start();
	}

	private void startCounter() {

		Task<Void> counter = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				// EST. TIME = [(processing time * generators) / servers] *
				// (push period / interval between tasks) + pushing period
				double maxProcessingTime = Double.parseDouble(maxProcessingTimeTextField.getText());
				double pushPeriod = (double) timeChooser.getValue();
				double numberOfServers = (double) nrOfServersComboBox.getValue();
				double numberOfGenerators = (double) nrOfTaskGeneratorsComboBox.getValue();
				double intervalBetweenTasks = (Double.parseDouble(minIntervalTextField.getText())
						+ Double.parseDouble(maxIntervalTextField.getText())) / 2;
				double estimatedTime = ((maxProcessingTime * numberOfGenerators) / numberOfServers)
						* (pushPeriod / intervalBetweenTasks) + pushPeriod;
				while (estimatedTime >= 0 && counting) {
					updateMessage((int) Math.floor(estimatedTime--) + " seconds");
					Thread.sleep(1000);
				}

				return null;
			}

		};
		remainingTimeLabel.textProperty().bind(counter.messageProperty());
		counterThread = new Thread(counter);
		counterThread.start();
	}

	private void updateServersStatus(String code) {
		char[] codeArray = new char[8];
		codeArray = code.toCharArray();
		int lineNumber;
		String processedTask;
		String message;
		HashMap<Integer, Integer> numberOfLinesPerServer = new HashMap<>();

		for (int serverIndex = 0; serverIndex < code.length(); serverIndex++) {
			int firstLineLength = ((TextArea) serversStatus.getChildren().get(serverIndex)).getLength();
			numberOfLinesPerServer.put(serverIndex, firstLineLength);
		}

		for (int serverIndex = 0; serverIndex < code.length(); serverIndex++) {
			lineNumber = numberOfLinesPerServer.get(serverIndex);
			TextArea textArea = ((TextArea) serversStatus.getChildren().get(serverIndex));
			textArea.setFont(new Font(10));

			if (codeArray[serverIndex] == '0') {
				message = "IDLE" + "\n";
			} else {
				processedTask = simulationManager.getServers().get(serverIndex).getCurrentTaskOnServer().getTaskName();
				LocalTime arrivalTime = simulationManager.getServers().get(serverIndex).getCurrentTaskOnServer()
						.getArrivalTime();
				message = "(" + arrivalTime.toString() + ")\n" + "PROCESSING " + processedTask.toUpperCase() + "\n";
			}

			if (!lastConsoleMessage.get(serverIndex).equals(message)) {
				textArea.insertText(lineNumber, message);
				lastConsoleMessage.put(serverIndex, message);
				numberOfLinesPerServer.put(serverIndex, ++lineNumber);
			}
		}
	}

	private void updateServersUI(String code) {
		VBox mainPanel = (VBox) this.rootPane.getCenter();
		HBox serversImage = (HBox) mainPanel.getChildren().get(0);
		ImageView serverView;
		for (int c = 0; c < code.length(); c++) {
			if (code.charAt(c) == '1') {
				/* Show a busy server */
				serverView = new ImageView(new Image("images/server-red.png"));
				serverView.setFitHeight(128);
				serverView.setFitWidth(128);
				serversImage.getChildren().set(c, serverView);
			} else {
				/* Show a idle server */
				serverView = new ImageView(new Image("images/server-green.png"));
				serverView.setFitHeight(128);
				serverView.setFitWidth(128);
				serversImage.getChildren().set(c, serverView);
			}
		}

		mainPanel.getChildren().set(0, serversImage);
		this.rootPane.setCenter(mainPanel);
	}

	private void stopSimulator() {
		disableMenu(false);
		startStopToggleButton.setSelected(false);
		showResults();
	}

	private void showResults() {
		Platform.runLater(() -> {
			
			averageProcessingTimeLabel.setText(Tracker.getInstance().getAverageProcessingTime() + " seconds");
			averageWaitingTimeLabel.setText(Tracker.getInstance().getAverageWaitingTime().getSeconds() + " seconds");
			XYChart.Series dataSeries = new XYChart.Series<>();
		
			dataSeries.setName("Current Session");
			CategoryAxis xAxis = new CategoryAxis();
			
			xAxis.setLabel("Servers");
			NumberAxis yAxis = new NumberAxis();
			yAxis.setTickUnit(1);
			yAxis.setLabel("Processed Tasks");
			
			BarChart<String, Number> chart = new BarChart<>(xAxis, yAxis);
			
			for(int thread = 0; thread < nrOfServersComboBox.getValue(); thread++) {

				String serverName = simulationManager.getServers().get(thread).getServerName();
				int processedTasks = simulationManager.getServers().get(thread).getNumberOfProcessedTasks();
				dataSeries.getData().add(new XYChart.Data(serverName, processedTasks));
			}
			chart.getData().add(dataSeries);
			chart.setLayoutX(38);
			chart.setLayoutY(89);
			chart.setPrefWidth(800);
			chart.setPrefHeight(247);
			chart.setLegendVisible(false);
			for(Node n: chart.lookupAll(".default-color0.chart-bar")) {
	            n.setStyle("-fx-bar-fill: #118c4e;");
	        }
			
			if(results.getChildren().size() == 5) {
				results.getChildren().set(4, chart);
			} else {
				results.getChildren().add(chart);
			}
			results.setVisible(true);
		});
		
	}

	private void stopSimulationNow() {
		simulationManager.getExecutor().shutdownNow();
		String offlineCode = "";
		switch (this.nrOfServersComboBox.getValue()) {
		case 2:
			offlineCode = "00";
			break;
		case 4:
			offlineCode = "0000";
			break;
		case 8:
			offlineCode = "00000000";
			break;

		}
		updateServersUI(offlineCode);

	}

	private void initializeMenu() {
		nrOfServersComboBox.getItems().add(2);
		nrOfServersComboBox.getItems().add(4);
		nrOfServersComboBox.getItems().add(8);

		nrOfTaskGeneratorsComboBox.getItems().add(2);
		nrOfTaskGeneratorsComboBox.getItems().add(4);
		nrOfTaskGeneratorsComboBox.getItems().add(8);
		
		results.setVisible(false);
	}

}
