package utcluj.tp.concurency;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import utcluj.tp.concurency.models.SimulationManager;



public class App extends Application {
	public static void main(String[] args) {
		launch(args);
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root; 
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml/Index.fxml"));
		root = loader.load();
		Scene scene = new Scene(root, 1336, 640);
		primaryStage.setScene(scene);
		primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
		primaryStage.setFullScreen(true);
		primaryStage.setResizable(false);
		primaryStage.setTitle("Concurency simulator");
		primaryStage.show();
	}
}